#include<stdio.h>
void swap(int *a,int *b);
int main()
{
    int a,b;
    printf("enter the numbers a and b \n");
    scanf("%d%d",&a,&b);
    printf("before swap a=%d and b=%d",a,b);
    swap(&a,&b);
    printf("after swap a=%d and b=%d",a,b);
    return 0;
}
 void swap(int *x,int *y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}
