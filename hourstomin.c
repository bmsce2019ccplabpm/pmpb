#include <stdio.h>
int input()
{
    int n;
    printf("Enter a number\n");
    scanf("%d",&n);
    return n;
}
int convert(int n)
{
    int r=n*60;
    return r;
}
void display(int n,int r)
{
    printf("%d hours is equal to %d minutes\n",n,r);
}
int main()
{
    int n = input();
    int r = convert(n);
    display(n,r);
    return 0;
}
