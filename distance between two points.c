#include <stdio.h>
#include <math.h>
int main()
{
    float x1,y1,x2,y2,gdistance;
    printf("enter x1\n");
    scanf("%f", &x1);
    printf("enter y1\n");
    scanf("%f", &y1);
    printf("enter x2\n");
    scanf("%f", &x2);
    printf("enter y2\n");
    scanf("%f", &y2);
    gdistance=((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
    printf("the distance between two points %.2f \n",sqrt(gdistance));
    printf("\n");
    return 0;
}
